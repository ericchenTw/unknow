#include "sharemem.h"

#include <string.h>
#include <errno.h>
#include <stdio.h>

#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>

#define  PATH_NAME "temp"

size_t FileSizeK;

char *MemPtr,*MemEltb,MemElem;

int create_new_file(size_t kbyte){
	char buff[1024]={0};
    int *memPtr,fid;
    FILE *fd;
	struct segment_header hd;
	
	hd.magic_number="ecshr";
	hd.write_lock=0;
	hd.pid=getpid();
	hd.capacity=kbyte;
	hd.access_control=7;
	
    fd = fopen(PATH_NAME,"rw");
    if (fd == NULL)
    {
        printf("open file %s failed...",PATH_NAME);
        return -1;
    }
	fwrite(buff,sizeof(buff),kbyte,fd);
	fseek(fd,0,SEEK_SET);
	fwrite(&hd,sizeof(struct segment_header),1,fd);
	fclose(fd);
	
	fid = open(PATH_NAME,O_RDWR);
    memPtr = (int *)mmap(NULL, 1024*kbyte, PROT_READ | PROT_WRITE, MAP_SHARED, fid, 0);
    if(fid == (void *)-1){
		printf("mmap fial . . .");
	}
    close(fid);
	
	fid=msync(memPtr,1024*kbyte,MS_SYNC);
    if (fid == -1)
    {
        printf("msync failed...");
        return -1;
    }
    
	FileSizeK=1024*kbyte;
	
	return 0;
}

int open_file(){
	int fid;
	FILE *fd;
	struct segment_header hd;
	
	fd = fopen(PATH_NAME,"rw");
    if (fd == NULL)
    {
        printf("open file %s failed...",PATH_NAME);
        return -1;
    }
	fread(&hd,sizeof(struct segment_header),1,fd);
	FileSizeK=hd.capacity;
	fclose(fd);
	
	fid = open(PATH_NAME,O_RDWR);
    memPtr = (int *)mmap(NULL, 1024*FileSizeK, PROT_READ | PROT_WRITE, MAP_SHARED, fid, 0);
    if(fid == (void *)-1){
		printf("mmap fial . . .");
	}
    close(fid);
    
	return 0; 
}

int main(){
	


	return 0;
}
