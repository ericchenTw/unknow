#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <time.h>
 
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/mman.h>
 
 
#define  PATH_NAME "temp"
 
int main()
{
    char buff[1024]={0};
    int *memPtr;
    FILE *fd;
    
 
    fd = fopen(PATH_NAME,"rw");
    if (fd == NULL)
    {
        printf("open file %s failed...",PATH_NAME);
        return -1;
    }
	fwrite(buff,sizeof(buff),8,fd);
	fclose(fd);
	
	int fid = open(PATH_NAME,O_RDWR);
    memPtr = (int *)mmap(NULL, 8192, PROT_READ | PROT_WRITE, MAP_SHARED, fid, 0);
    close(fid);
    
 
    if (memPtr == MAP_FAILED)
    {
        printf("mmap failed...");
        return -1;
    }
    for(int i=0;i<26;i++,memPtr=memPtr+1){
		(*memPtr)=i+'A';
		printf("write:%c at:%p\n",*memPtr,memPtr);
    }
    fid=msync(memPtr-26,8192,MS_SYNC);
    if (fid == -1)
    {
        printf("msync failed...");
        return -1;
    }
	printf("process:%d send:%c\n",getpid(),*memPtr);
	sleep(20); 
    return 0;
}

