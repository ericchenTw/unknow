#include "sharemem.h"
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#define targetfilename "temp"


fpos_t Mempos,Meme_tablepos,Memelementspos;

int init_segment_header(struct segment_header *h,int kbytes,int access_control){
	h->magic_number[0]='e';
	h->write_lock=0;
	h->pid=(int)getpid();
	h->capacity=kbytes;
	h->access_control=access_control&7;
	
	return 1;
}

int createfile(size_t kbytes){
	char buff[1024];
	memset(buff,0,1024);
	FILE *f=fopen(targetfilename,"w");
	if(f == NULL){return 0;}
	setvbuf(f,NULL,_IONBF,0);
	for(int i=0;i<kbytes;++i){
		fwrite(buff,sizeof(char)*1024,1,f);
	}
	struct segment_header h;
	init_segment_header(&h,kbytes,7);
	fseek(f,0,SEEK_SET);
	fwrite(&h,sizeof(struct segment_header),1,f);
	fclose(f);
	return 1;
}
int memopen(){
	FILE *fp=fopen(targetfilename,"rw");
	char buff[65535];
	fgetpos(fp,&Mempos);
	fread(buff,sizeof(struct segment_header),1,fp);
	fgetpos(fp,&Meme_tablepos);
	fread(buff,sizeof(struct element_table_u),element_table_len,fp);
	fgetpos(fp,&Memelementspos);
	return 1;
}
int writevar_0(size_t ele,void *v){
	FILE *fp;
	char buff[4096],buff1[4096];
	struct element_table_u tu;
	int etype;
	fsetpos(fp,&Memelementspos);
	for(;;){
		fread(buff,sizeof(struct element_number),1,fp);
		etype=(int)buff[0]&(128+64+32);
		if(etype == 0){
			tu.element=ftell(fp)-sizeof(struct element_number);
			fseek(fp,ftell(fp)-sizeof(struct element_number),SEEK_SET);
			fwrite(v,sizeof(struct element_number),1,fp);
			break;
		}
	}
	fsetpos(fp,&Meme_tablepos);
	tu.symbol=0;
	fread(buff1,sizeof(struct element_table_u),ele,fp); //ack
	fwrite(&tu,sizeof(struct element_table_u),1,fp);
	
	return 0;
}
void* readvar_0(size_t ele){
	FILE *fp;
	char buff[4096],buff1[65535];
	struct element_table_u tu;
	struct element_number en;
	fsetpos(fp,&Memelementspos);
	fread(buff1,sizeof(struct element_table_u),ele,fp);
	fread(&tu,sizeof(struct element_table_u),1,fp);
	fseek(fp,tu.element,SEEK_SET);
	fread(&en,sizeof(struct element_number),1,fp);
	return (void *)(&en);
}
int main(){
	struct element_number en,*enp;
	createfile(8);
	memopen();
	//writevar_0(0,&en);
	//enp=(struct element_number*)readvar_0(0);

	return 0;
}
